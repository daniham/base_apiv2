<?php
$link1 = mysqli_connect("localhost", "root", "");
mysqli_select_db($link1, "klhk");
if (!$link1)
    echo "Error : " . mysqli_error($link1);
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap4.min.css">
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Uji konektivitas</title>
</head>

<body>

    <div class="row mt-5 ml-2 mr-3">
        <table id="example" class="table table-striped table-bordered dt-responsive" style="width:100%">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Waktu </th>
                    <th>PM10</th>
                    <th>PM25</th>
                    <th>SO2</th>
                    <th>O3</th>
                    <th>NO2</th>
                    <th>HC</th>
                    <th>WD.</th>
                    <th>STAT PM10</th>
                    <th>STAT PM25</th>
                    <th>STAT SO2</th>
                    <th>STAT CO</th>
                    <th>STAT NO2</th>
                    <th>STAT HC</th>
                    <th>HUM</th>
                    <th>TEMP</th>
                    <th>PRESS</th>
                    <th>SR</th>
                    <th>RAIN</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $ambil =  mysqli_query($link1, "SELECT * FROM `base_klhk`");
                while ($row = mysqli_fetch_array($ambil)) {
                ?>
                    <tr>

                        <th><?= $no++; ?></th>
                        <th><?= $row['waktu']; ?></th>
                        <th><?= $row['pm10']; ?></th>
                        <th><?= $row['pm25']; ?></th>
                        <th><?= $row['so2']; ?></th>
                        <th><?= $row['o3']; ?></th>
                        <th><?= $row['no2']; ?></th>
                        <th><?= $row['hc']; ?></th>
                        <th><?= $row['wd']; ?></th>
                        <th><?= $row['stat_pm10']; ?></th>
                        <th><?= $row['stat_pm25']; ?></th>
                        <th><?= $row['stat_so2']; ?></th>
                        <th><?= $row['stat_co']; ?></th>
                        <th><?= $row['stat_no2']; ?></th>
                        <th><?= $row['stat_hc']; ?></th>
                        <th><?= $row['humidity']; ?></th>
                        <th><?= $row['temperatur']; ?></th>
                        <th><?= $row['pressure']; ?></th>
                        <th><?= $row['sr']; ?></th>
                        <th><?= $row['rain_intensity']; ?></th>

                    </tr>
                <?php } ?>

            </tbody>
        </table>

    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
</body>


</html> ä