<?php
class Rth{
// dbection
private $db;
// Table
private $db_table = "_gas_rth";
private $base = "";
// Columns
public $_id;
public $waktu;
public $t_so2;
public $t_co;
public $t_o3;
public $t_no2;
public $h_so2;
public $h_co;
public $h_o3;
public $h_no2;
public $stat_conn;
public $result;


// Db dbection
public function __construct($db){
    $this->db = $db;
}
// GET ALL
public function getRth(){
    $sqlQuery = "SELECT waktu, t_so2,t_co,t_o3,t_no2,h_so2,h_co,h_o3,h_no2,stat_conn FROM " . $this->db_table = "demo_gas_rth" . "";
    $this->result = $this->db->query($sqlQuery);
    return $this->result;
}

// POST
public function createRth(){
// sanitize
    if ($this->base=="oki" || $this->base=="oi" || $this->base=="serang" || $this->base=="ambon" || $this->base=="manokwari" || $this->base=="palu" || $this->base=="ternate" || $this->base=="gorontalo" || $this->base=="kendari" || $this->base=="samarinda" || $this->base=="mamuju" || $this->base=="pangkal_pinang" || $this->base=="demo") {

        $this->waktu=htmlspecialchars(strip_tags($this->waktu));
        $this->t_so2=htmlspecialchars(strip_tags($this->t_so2));
        $this->t_co=htmlspecialchars(strip_tags($this->t_co));
        $this->t_o3=htmlspecialchars(strip_tags($this->t_o3));
        $this->t_no2=htmlspecialchars(strip_tags($this->t_no2));
        $this->h_so2=htmlspecialchars(strip_tags($this->h_so2));
        $this->h_co=htmlspecialchars(strip_tags($this->h_co));
        $this->h_o3=htmlspecialchars(strip_tags($this->h_o3));
        $this->h_no2=htmlspecialchars(strip_tags($this->h_no2));
        $this->stat_conn=htmlspecialchars(strip_tags($this->stat_conn));
       
       $tb=$base.$db_table;
       $sqlQuery = "INSERT INTO
            ". $tb ." SET waktu = '".$this->waktu."', t_so2 = '".$this->t_so2."',
            t_co = '".$this->t_co."',t_o3 = '".$this->t_o3."',t_no2 = '".$this->t_no2."',
            h_so2 = '".$this->h_so2."',h_co = '".$this->h_co."',h_o3 = '".$this->h_o3."',h_no2 = '".$this->h_no2."',
            stat_conn = '".$this->stat_conn."'";
        $this->db->query($sqlQuery);
        if($this->db->affected_rows > 0){
            return true;
        }
            return false;
    }


}

// GET by Params
public function getSingleRth(){
    $sqlQuery = "SELECT * FROM
    ". $this->db_table = "demo_gas_rth" ." WHERE _id = ".$this->_id;
    $record = $this->db->query($sqlQuery);
    $dataRow=$record->fetch_assoc();
    $this->id = $dataRow['id'];
    $this->waktu = $dataRow['waktu'];
    $this->t_so2 = $dataRow['t_so2'];
    $this->t_co = $dataRow['t_co'];
    $this->t_o3 = $dataRow['t_o3'];
    $this->t_no2 = $dataRow['t_no2'];
    $this->h_so2 = $dataRow['h_so2'];
    $this->h_co = $dataRow['h_co'];
    $this->h_o3 = $dataRow['h_o3'];
    $this->h_no2 = $dataRow['h_no2'];
    $this->stat_conn = $dataRow['stat_conn'];
}
// PUT
public function updateRth(){
    $this->waktu=htmlspecialchars(strip_tags($this->waktu));
    $this->t_so2=htmlspecialchars(strip_tags($this->t_so2));
    $this->t_co=htmlspecialchars(strip_tags($this->t_co));
    $this->t_o3=htmlspecialchars(strip_tags($this->t_o3));
    $this->t_no2=htmlspecialchars(strip_tags($this->t_no2));
    $this->h_so2=htmlspecialchars(strip_tags($this->h_so2));
    $this->h_co=htmlspecialchars(strip_tags($this->h_co));
    $this->h_o3=htmlspecialchars(strip_tags($this->h_o3));
    $this->h_no2=htmlspecialchars(strip_tags($this->h_no2));
    $this->stat_conn=htmlspecialchars(strip_tags($this->stat_conn));
    $this->_id=htmlspecialchars(strip_tags($this->_id));

    $sqlQuery = "UPDATE ". $this->db_table = "demo_gas_rth"  ." SET waktu = '".$this->waktu."', t_so2 = '".$this->t_so2."',
    t_co = '".$this->t_co."',t_o3 = '".$this->t_o3."',t_no2 = '".$this->t_no2."',
    h_so2 = '".$this->h_so2."',h_co = '".$this->h_co."',h_o3 = '".$this->h_o3."',h_no2 = '".$this->h_no2."',
    stat_conn = '".$this->stat_conn."'
    WHERE _id = ".$this->_id;

    $this->db->query($sqlQuery);
    if($this->db->affected_rows > 0){
        return true;
    }
        return false;
}

// DELETE
function deleteRth(){
    $sqlQuery = "DELETE FROM " . $this->db_table = "demo_gas_rth"  . " WHERE _id = ".$this->_id;
    $this->db->query($sqlQuery);
    if($this->db->affected_rows > 0){
        return true;
    }
    return false;
    }
}
?>
