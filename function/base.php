<?php
class Base{
// dbection
private $db;
// Table
private $db_table = "";
// Columns
public $id_stasiun;
public $waktu;
public $pm10;
public $pm25;
public $so2;
public $co;
public $o3;
public $no2;
public $hc;
public $ws;
public $wd;
public $stat_pm10;
public $stat_pm25;
public $stat_so2;
public $stat_co;
public $stat_o3;
public $stat_no2;
public $stat_hc;
public $humidity;
public $temperatur;
public $pressure;
public $sr;
public $rain_intensity;
public $feedback;
public $stat_conn;
public $fb_secure;
public $keynumber;
public $key;
public $result;


// Db dbection
public function __construct($db){
$this->db = $db;
}

// GET ALL
public function getBase(){
$sqlQuery = "SELECT * FROM " . $this->db_table = "base_klhk" . "";
$this->result = $this->db->query($sqlQuery);
return $this->result;
}
// POST
public function createBase(){
// sanitize
    $this->id_stasiun=htmlspecialchars(strip_tags($this->id_stasiun));
    $this->waktu=htmlspecialchars(strip_tags($this->waktu));
    $this->pm10=htmlspecialchars(strip_tags($this->pm10));
    $this->pm25=htmlspecialchars(strip_tags($this->pm25));
    $this->so2=htmlspecialchars(strip_tags($this->so2));
    $this->co=htmlspecialchars(strip_tags($this->co));
    $this->o3=htmlspecialchars(strip_tags($this->o3));
    $this->no2=htmlspecialchars(strip_tags($this->no2));
    $this->hc=htmlspecialchars(strip_tags($this->hc));
    $this->ws=htmlspecialchars(strip_tags($this->ws));
    $this->wd=htmlspecialchars(strip_tags($this->wd));
    $this->stat_pm10=htmlspecialchars(strip_tags($this->stat_pm10));
    $this->stat_pm25=htmlspecialchars(strip_tags($this->stat_pm25));
    $this->stat_so2=htmlspecialchars(strip_tags($this->stat_so2));
    $this->stat_co=htmlspecialchars(strip_tags($this->stat_co));
    $this->stat_o3=htmlspecialchars(strip_tags($this->stat_o3));
    $this->stat_no2=htmlspecialchars(strip_tags($this->stat_no2));
    $this->stat_hc=htmlspecialchars(strip_tags($this->stat_hc));
    $this->humidity=htmlspecialchars(strip_tags($this->humidity));
    $this->temperatur=htmlspecialchars(strip_tags($this->temperatur));
    $this->pressure=htmlspecialchars(strip_tags($this->pressure));
    $this->sr=htmlspecialchars(strip_tags($this->sr));
    $this->rain_intensity=htmlspecialchars(strip_tags($this->rain_intensity));
    $this->feedback=htmlspecialchars(strip_tags($this->feedback));
    $this->fb_secure=htmlspecialchars(strip_tags($this->fb_secure));
    $this->stat_conn=htmlspecialchars(strip_tags($this->stat_conn));
    $this->keynumber=htmlspecialchars(strip_tags($this->keynumber));
    $sqlQuery = "INSERT INTO
        ". $this->db_table = "base_klhk" ." SET  id_stasiun = '".$this->id_stasiun."',waktu = '".$this->waktu."', pm10 = '".$this->pm10."', pm25 = '".$this->pm25."',
        so2 = '".$this->so2."',co = '".$this->co."',o3 = '".$this->o3."',no2 = '".$this->no2."',hc = '".$this->hc."',
        ws = '".$this->ws."',wd = '".$this->wd."',stat_pm10 = '".$this->stat_pm10."',
        stat_pm25 = '".$this->stat_pm25."',stat_so2 = '".$this->stat_so2."',stat_co = '".$this->stat_co."',
        stat_o3 = '".$this->stat_o3."',stat_no2 = '".$this->stat_no2."',stat_hc = '".$this->stat_hc."',
        humidity = '".$this->humidity."',temperatur = '".$this->temperatur."',
        pressure = '".$this->pressure."',sr = '".$this->sr."'
        ,rain_intensity = '".$this->rain_intensity."',feedback = '".$this->feedback."',
        stat_conn = '".$this->stat_conn."',fb_secure = '".$this->fb_secure."',keynumber = '".$this->keynumber."'";
    $this->db->query($sqlQuery);
    if($this->db->affected_rows > 0){
        return true;
    }
        return false;
}

// GET by Params
public function getSingleBase(){
    $sqlQuery = "SELECT * FROM
    ". $this->db_table = "base_klhk" ." WHERE id_stasiun = ".$this->id_stasiun;
    $record = $this->db->query($sqlQuery);
    $dataRow=$record->fetch_assoc();
    $this->id_stasiun = $dataRow['id_stasiun'];
    $this->waktu = $dataRow['waktu'];
    $this->pm10 = $dataRow['pm10'];
    $this->pm25 = $dataRow['pm25'];
    $this->so2 = $dataRow['so2'];
    $this->co = $dataRow['co'];
    $this->o3 = $dataRow['o3'];
    $this->no2 = $dataRow['no2'];
    $this->hc = $dataRow['hc'];
    $this->stat_pm10 = $dataRow['stat_pm10'];
    $this->stat_pm25 = $dataRow['stat_pm25'];
    $this->stat_so2 = $dataRow['stat_so2'];
    $this->stat_co = $dataRow['stat_co'];
    $this->stat_o3 = $dataRow['stat_o3'];
    $this->stat_no2 = $dataRow['stat_no2'];
    $this->stat_hc = $dataRow['stat_hc'];
    $this->ws = $dataRow['ws'];
    $this->wd = $dataRow['wd'];
    $this->humidity = $dataRow['humidity'];
    $this->temperatur = $dataRow['temperatur'];
    $this->pressure = $dataRow['pressure'];
    $this->sr = $dataRow['sr'];
    $this->rain_intensity = $dataRow['rain_intensity'];
}

// PUT
public function updateBase(){
    $this->waktu=htmlspecialchars(strip_tags($this->waktu));
    $this->pm10=htmlspecialchars(strip_tags($this->pm10));
    $this->pm25=htmlspecialchars(strip_tags($this->pm25));
    $this->so2=htmlspecialchars(strip_tags($this->so2));
    $this->co=htmlspecialchars(strip_tags($this->co));
    $this->o3=htmlspecialchars(strip_tags($this->o3));
    $this->no2=htmlspecialchars(strip_tags($this->no2));
    $this->hc=htmlspecialchars(strip_tags($this->hc));
    $this->ws=htmlspecialchars(strip_tags($this->ws));
    $this->wd=htmlspecialchars(strip_tags($this->wd));
    $this->stat_pm10=htmlspecialchars(strip_tags($this->stat_pm10));
    $this->stat_pm25=htmlspecialchars(strip_tags($this->stat_pm25));
    $this->stat_so2=htmlspecialchars(strip_tags($this->stat_so2));
    $this->stat_co=htmlspecialchars(strip_tags($this->stat_co));
    $this->stat_o3=htmlspecialchars(strip_tags($this->stat_o3));
    $this->stat_no2=htmlspecialchars(strip_tags($this->stat_no2));
    $this->stat_hc=htmlspecialchars(strip_tags($this->stat_hc));
    $this->humidity=htmlspecialchars(strip_tags($this->humidity));
    $this->temperatur=htmlspecialchars(strip_tags($this->temperatur));
    $this->pressure=htmlspecialchars(strip_tags($this->pressure));
    $this->sr=htmlspecialchars(strip_tags($this->sr));
    $this->rain_intensity=htmlspecialchars(strip_tags($this->rain_intensity));
    $this->id_stasiun=htmlspecialchars(strip_tags($this->id_stasiun));

    $sqlQuery = "UPDATE ". $this->db_table = "base_klhk"  ." SET waktu = '".$this->waktu."', pm10 = '".$this->pm10."', pm25 = '".$this->pm25."',
    so2 = '".$this->so2."',co = '".$this->co."',o3 = '".$this->o3."',no2 = '".$this->no2."',hc = '".$this->hc."',
    ws = '".$this->ws."',wd = '".$this->wd."',stat_pm10 = '".$this->stat_pm10."',
    stat_pm25 = '".$this->stat_pm25."',stat_so2 = '".$this->stat_so2."',stat_co = '".$this->stat_co."',
    stat_o3 = '".$this->stat_o3."',stat_no2 = '".$this->stat_no2."',stat_hc = '".$this->stat_hc."',
    humidity = '".$this->humidity."',temperatur = '".$this->temperatur."',
    pressure = '".$this->pressure."'sr = '".$this->sr."',rain_intensity = '".$this->rain_intensity."'
    WHERE id_stasiun = ".$this->id_stasiun;

    $this->db->query($sqlQuery);
    if($this->db->affected_rows > 0){
        return true;
    }
        return false;
}

// DELETE
function deleteBase(){
    $sqlQuery = "DELETE FROM " . $this->db_table = "base_klhk"  . " WHERE id_stasiun = ".$this->id_stasiun;
    $this->db->query($sqlQuery);
    if($this->db->affected_rows > 0){
        return true;
    }
    return false;
    }
}
?>
