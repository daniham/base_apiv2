<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
date_default_timezone_set('Asia/Jakarta');

include_once '../config/conn.php';
include_once '../function/base.php';
$database = new Database();
$db = $database->getConnection();
$connecting = $database->is_connected();
// $secretkeys = file_get_contents('http://secure.getsensync.com/sparing_new/api/try.php');
$secretkeys = file_get_contents('http://localhost/base_apiv2/api/secret.php');

$uid=2000000000016;
$item = new Base($db);

//test conn fb_secure
$ch3 = curl_init();
curl_setopt($ch3, CURLOPT_URL, 'http://localhost/base_apiv2/handling/status.php');
curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch3, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch3, CURLOPT_POST, 1);
curl_setopt($ch3, CURLOPT_POSTFIELDS,$temp_arr);
curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false); 
curl_setopt($ch3, CURLOPT_SSL_VERIFYHOST, false);
$resultss = curl_exec($ch3);
$responses = json_decode($resultss, true);
$stats=$responses['status'];

//kirim data ke server uji
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://203.166.207.50/api/server-uji');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query(array('token' => $jwt)));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($ch);
//jwt encrypt
$header = json_encode(['typ'=>'JWT','alg'=>'HS256']);
$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

$params = array(
        'uid' => $uid,
		"id_stasiun"=>$item->id_stasiun = $_GET['id_stasiun'],
        "waktu" => date('Y-m-d H:i:s'),
        "pm10"=>$item->pm10 = $_GET['pm10'],
        "pm25"=>$item->pm25 = $_GET['pm25'],
        "so2"=>$item->so2 = $_GET['so2'],
        "co"=>$item->co = $_GET['co'],
        "o3"=>$item->o3 = $_GET['o3'],
        "no2"=>$item->no2 = $_GET['no2'],
        "hc"=>$item->hc = $_GET['hc'],
        "ws"=>$item->ws = $_GET['ws'],
        "wd"=>$item->wd = $_GET['wd'],
        "stat_pm10"=>1,
        "stat_pm25"=>1,
        "stat_so2"=>1,
        "stat_co"=>1,
        "stat_o3"=>1,
        "stat_no2"=>1,
        "stat_hc"=>1,
        "humidity"=>$item->humidity = $_GET['humidity'],
        "temperatur"=>$item->temperatur = $_GET['temperatur'],
        "pressure"=>$item->$pressure = $_GET['$pressure'],
        "sr"=>$item->sr = $_GET['sr'],
        "rain_intensity"=>$item->rain_intensity = $_GET['rain_intensity'],
);

$payload = json_encode($params);
$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));	
$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload,$secretkeys, true);
$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature)); 
$jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

//item value property dari post
$item->id_stasiun = "Sensync";
// $item->id_stasiun = $_GET['id_stasiun'];
$item->waktu = date('Y-m-d H:i:s');
$item->pm10 = $_GET['pm10'];
$item->pm25 = $_GET['pm25'];
$item->so2 = $_GET['so2'];
$item->co = $_GET['co'];
$item->o3 = $_GET['o3'];
$item->no2 = $_GET['no2'];
$item->hc = $_GET['hc'];
$item->ws = $_GET['ws'];
$item->wd = $_GET['wd'];
$item->stat_pm10 =1;
$item->stat_pm25 =1;
$item->stat_so2 =1;
$item->stat_co =1;
$item->stat_o3 =1;
$item->stat_no2 =1;
$item->stat_hc =1;
$item->humidity = $_GET['humidity'];
$item->temperatur = $_GET['temperatur'];
$item->pressure = $_GET['pressure'];
$item->sr = $_GET['sr'];
$item->rain_intensity = $_GET['rain_intensity'];
$item->feedback ="sukses";
$item->stat_conn =$connecting;
$item->fb_secure = $stats ;
$item->keynumber = $jwt ;
$item->key = $secretkeys; 
$current = date('d M Y H:i:s');

//insert and encription data to jwt
$query="INSERT INTO encrypt (waktu ,keynumber,stat_conn,feedback) VALUES ('$item->waktu','$item->keynumber','$connecting','$item->feedback')";
$inserts=mysqli_query($db,$query);

//update log secretkey
$querys="UPDATE secretkey SET waktu='$item->waktutime',`key`='$item->key' WHERE id=1";
$insertsc=mysqli_query($db,$querys);

if($item->createBase()){
	// echo json_encode(array('token'  => $jwt ));
	echo json_encode(array('status' => 1 , 'Message' => "Sukses" , 'Current Time'  => $current ));
	echo json_encode('Data created successfully.');
} else{
	echo json_encode(array('status' => 0 , 'Message' => "Gagal" , 'Current Time'  => $current ));
	echo json_encode('Data could not be created.');
}
?>