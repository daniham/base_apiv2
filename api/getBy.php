<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
date_default_timezone_set('Asia/Jakarta');

include_once '../config/conn.php';
include_once '../function/base.php';

$database = new Database();
$db = $database->getConnection();
$item = new Base($db);
$item->id_stasiun = isset($_GET['id_stasiun']) ? $_GET['id_stasiun'] : die();

$item->getSingleBase();
$current = date('d M Y H:i:s');

if($item->waktu != null){
    $data = array();

// create array
$emp_arr = array(
    "id_stasiun"=>$item->id_stasiun,
    "waktu" => $item->waktu,
    "pm10"=>$item->pm10,
    "pm25"=>$item->pm25,
    "so2"=>$item->so2,
    "co"=>$item->co,
    "o3"=>$item->o3,
    "no2"=>$item->no2,
    "hc"=>$item->hc,
    "ws"=>$item->ws,
    "wd"=>$item->wd,
    "stat_pm10"=>$item->stat_pm10,
    "stat_pm25"=>$item->stat_pm25,
    "stat_so2"=>$item->stat_so2,
    "stat_co"=>$item->stat_co,
    "stat_o3"=>$item->stat_o3,
    "stat_no2"=>$item->stat_no2,
    "stat_hc"=>$item->stat_hc,
    "humidity"=>$item->humidity,
    "temperatur"=>$item->temperatur,
    "pressure"=>$item->pressure,
    "sr"=>$item->sr,
    "rain_intensity"=>$item->rain_intensity
);

$current = date('d M Y H:i:s');
$data = ['status' => 1 , 'Message' => "Sukses" ,'data' => $emp_arr ,'Current Time'  => $current ];

http_response_code(200);
echo json_encode($data);
}
else{
http_response_code(404);
echo json_encode(array('status' => 0 , 'Message' => "Gagal" , 'Current Time'  => $current ));
}
?>