<?php
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include_once '../../config/conn.php';
include_once '../../function/rth_func.php';

$database = new Database();
$db = $database->getConnection();
$connecting = $database->is_connected();

$item = new Rth($db);

$item->waktu = date('Y-m-d H:i:s');
$item->t_so2 = $_GET['t_so2'];
$item->t_co = $_GET['t_co'];
$item->t_o3 = $_GET['t_o3'];
$item->t_no2 = $_GET['t_no2'];
$item->h_so2 = $_GET['h_so2'];
$item->h_co = $_GET['h_co'];
$item->h_o3 = $_GET['h_o3'];
$item->h_no2 = $_GET['h_no2'];
$item->stat_conn =$connecting;
$current = date('d M Y H:i:s');

$sends['base'] = $base;
$sends['t_so2'] = $t_so2;
$sends['t_co'] = $t_co;
$sends['t_o3'] = $t_o3;
$sends['t_no2'] = $t_no2;
$sends['h_so2'] = $h_so2;
$sends['h_co'] = $h_co;
$sends['h_o3'] = $h_o3;
$sends['h_no2'] = $h_no2;
$query23 = http_build_query($sends);
$curl23 = curl_init();
curl_setopt_array($curl23, array(
CURLOPT_RETURNTRANSFER => 1,
CURLOPT_URL => 'http://secure.getsensync.com/KLHK2020/Insertrth.php?'.$query23,
));
$resp23 = curl_exec($curl23);
curl_close($curl23);

if($item->createRth()){
	// echo json_encode(array('token'  => $jwt ));
	echo json_encode(array('status' => 1 , 'Message' => "Sukses" , 'Current Time'  => $current ));
	// echo json_encode('Data created successfully.');
} else{
	echo json_encode(array('status' => 0 , 'Message' => "Gagal" , 'Current Time'  => $current ));
	// echo json_encode('Data could not be created.');
}
?>