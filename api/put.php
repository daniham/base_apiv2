<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
date_default_timezone_set('Asia/Jakarta');

include_once '../config/conn.php';
include_once '../function/base.php';

$database = new Database();
$db = $database->getConnection();
$item = new Base($db);

$item->id_stasiun = isset($_GET['id_stasiun']) ? $_GET['id_stasiun'] : die();

$item->waktu = date('Y-m-d H:i:s');
$item->pm10 = $_GET['pm10'];
$item->pm25 = $_GET['pm25'];
$item->so2 = $_GET['so2'];
$item->co = $_GET['co'];
$item->o3 = $_GET['o3'];
$item->no2 = $_GET['no2'];
$item->hc = $_GET['hc'];
$item->ws = $_GET['ws'];
$item->wd = $_GET['wd'];

if ($item->waktu != null){
    $item->stat_pm10 =1;
    $item->stat_pm25 =1;
    $item->stat_so2 =1;
    $item->stat_co =1;
    $item->stat_o3 =1;
    $item->stat_no2 =1;
    $item->stat_hc =1;
}else{
    $item->stat_pm10 =0;
    $item->stat_pm25 =0;
    $item->stat_so2 =0;
    $item->stat_co =0;
    $item->stat_o3 =0;
    $item->stat_no2 =0;
    $item->stat_hc =0;
}
// $item->stat_pm10 = $_GET['stat_pm10'];
// $item->stat_pm25 = $_GET['stat_pm25'];
// $item->stat_so2 = $_GET['stat_so2'];
// $item->stat_co = $_GET['stat_co'];
// $item->stat_o3 = $_GET['stat_o3'];
// $item->stat_no2 = $_GET['stat_no2'];
// $item->stat_hc = $_GET['stat_hc'];
$item->humidity = $_GET['humidity'];
$item->temperatur = $_GET['temperatur'];
$item->pressure = $_GET['pressure'];
$item->sr = $_GET['sr'];
$item->rain_intensity = $_GET['rain_intensity'];

$current = date('d M Y H:i:s');

if($item->updateBase()){
	echo json_encode(array('status' => 1 , 'Message' => "Sukses" , 'Current Time'  => $current ));
    echo json_encode("Data updated successfully.");
} else{
	echo json_encode(array('status' => 0 , 'Message' => "Gagal" , 'Current Time'  => $current ));
    echo json_encode("Data could not be updated");
}
?>