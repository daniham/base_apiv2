<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
date_default_timezone_set('Asia/Jakarta');

include_once '../config/conn.php';
include_once '../function/base.php';
$database = new Database();
$db = $database->getConnection();
$item = new Base($db);

$item->id_stasiun = isset($_GET['id_stasiun']) ? $_GET['id_stasiun'] : die();
$current = date('d M Y H:i:s');

if($item->deleteBase()){
	echo json_encode(array('Status' => 1 , 'Message' => "Sukses" , 'Current Time'  => $current ));
    echo json_encode("Data deleted Succesfully.");
} else{
	echo json_encode(array('Status' => 0 , 'Message' => "Gagal" , 'Current Time'  => $current ));
    echo json_encode("Data could not be deleted");
}
?>