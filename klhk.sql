-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 27, 2021 at 06:20 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klhk`
--

-- --------------------------------------------------------

--
-- Table structure for table `base_klhk`
--

CREATE TABLE `base_klhk` (
  `id_stasiun` varchar(50) NOT NULL,
  `waktu` datetime DEFAULT NULL,
  `pm10` float DEFAULT NULL,
  `pm25` float DEFAULT NULL,
  `so2` float DEFAULT NULL,
  `co` float DEFAULT NULL,
  `o3` float DEFAULT NULL,
  `no2` float DEFAULT NULL,
  `hc` float DEFAULT NULL,
  `ws` float DEFAULT NULL,
  `wd` float DEFAULT NULL,
  `stat_pm10` int(11) DEFAULT NULL,
  `stat_pm25` int(11) DEFAULT NULL,
  `stat_so2` int(11) DEFAULT NULL,
  `stat_co` int(11) DEFAULT NULL,
  `stat_o3` int(11) DEFAULT NULL,
  `stat_no2` int(11) DEFAULT NULL,
  `stat_hc` int(11) DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `temperatur` float DEFAULT NULL,
  `pressure` float DEFAULT NULL,
  `sr` float DEFAULT NULL,
  `rain_intensity` float DEFAULT NULL,
  `feedback` varchar(50) DEFAULT NULL,
  `stat_conn` int(11) DEFAULT NULL,
  `fb_secure` int(11) DEFAULT NULL,
  `keynumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `base_klhk`
--

INSERT INTO `base_klhk` (`id_stasiun`, `waktu`, `pm10`, `pm25`, `so2`, `co`, `o3`, `no2`, `hc`, `ws`, `wd`, `stat_pm10`, `stat_pm25`, `stat_so2`, `stat_co`, `stat_o3`, `stat_no2`, `stat_hc`, `humidity`, `temperatur`, `pressure`, `sr`, `rain_intensity`, `feedback`, `stat_conn`, `fb_secure`, `keynumber`) VALUES
('sensync3', '2021-04-27 09:26:00', 13, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('sensync3', '2021-04-27 09:45:35', 11, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('sensync3', '2021-04-27 09:48:52', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('sensync', '2021-04-27 09:50:21', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('sensync', '2021-04-27 09:51:26', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('sensyncx', '2021-04-27 09:52:14', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('aaaaa', '2021-04-27 09:54:20', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('zzz', '2021-04-27 09:57:53', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('sasa', '2021-04-27 10:02:27', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('qwe', '2021-04-27 10:04:03', 123, 12, 10, 15, 12, 12, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('qwe', '2021-04-27 10:04:48', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('qwe', '2021-04-27 10:05:37', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('werrrt', '2021-04-27 10:07:33', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('wrttt', '2021-04-27 10:10:03', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, ''),
('tubagus', '2021-04-27 10:12:42', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJ0dWJhZ3VzIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjEyOjQyIiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.k2Ejggz1UsgPmieparv99aRFpb2NtHiKbu8l8XtcrZs'),
('ismail', '2021-04-27 10:18:31', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJpc21haWwiLCJ3YWt0dSI6IjIwMjEtMDQtMjcgMTA6MTg6MzEiLCJwbTEwIjoiMTIzIiwicG0yNSI6IjEyIiwic28yIjoiMTAiLCJjbyI6IjE1IiwibzMiOiIxMiIsIm5vMiI6IjMyMjEiLCJoYyI6IjEyIiwid3MiOiIxMiIsIndkIjoiMjMyIiwic3RhdF9wbTEwIjpudWxsLCJzdGF0X3BtMjUiOm51bGwsInN0YXRfc28yIjpudWxsLCJzdGF0X2NvIjpudWxsLCJzdGF0X28zIjpudWxsLCJzdGF0X25vMiI6bnVsbCwic3RhdF9oYyI6bnVsbCwiaHVtaWRpdHkiOiIxIiwidGVtcGVyYXR1ciI6IjEiLCJwcmVzc3VyZSI6bnVsbCwic3IiOiIxMSIsInJhaW5faW50ZW5zaXR5IjoiMTEifQ.ll1OlTKzruPMYpjcOd0_InQlnWQbE155qE7A9a-wA3E'),
('ismailmarzuki', '2021-04-27 10:23:02', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJpc21haWxtYXJ6dWtpIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjIzOjAyIiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.bn-jw7_1Xt80Vifs6N59hpjxBiiAZz5RMeSF6EGyL1I'),
('ismailmarzukiali', '2021-04-27 10:24:16', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJpc21haWxtYXJ6dWtpYWxpIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjI0OjE2IiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.Vr4KIDb4BQK1opFt24IVp2sNdSchZExaLXMs1eqQHck'),
('dago', '2021-04-27 10:26:48', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJkYWdvIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjI2OjQ4IiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.LxndgeAKU0f4kL-BuHR3jIcXQNxqYWnzdaojBj6HrxA'),
('Sensync', '2021-04-27 11:03:28', 123, 12, 10, 15, 12, 3221, 12, 12, 232, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 'sukses', 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOm51bGwsIndha3R1IjoiMjAyMS0wNC0yNyAxMTowMzoyOCIsInBtMTAiOiIxMjMiLCJwbTI1IjoiMTIiLCJzbzIiOiIxMCIsImNvIjoiMTUiLCJvMyI6IjEyIiwibm8yIjoiMzIyMSIsImhjIjoiMTIiLCJ3cyI6IjEyIiwid2QiOiIyMzIiLCJzdGF0X3BtMTAiOm51bGwsInN0YXRfcG0yNSI6bnVsbCwic3RhdF9zbzIiOm51bGwsInN0YXRfY28iOm51bGwsInN0YXRfbzMiOm51bGwsInN0YXRfbm8yIjpudWxsLCJzdGF0X2hjIjpudWxsLCJodW1pZGl0eSI6IjEiLCJ0ZW1wZXJhdHVyIjoiMSIsInByZXNzdXJlIjpudWxsLCJzciI6IjExIiwicmFpbl9pbnRlbnNpdHkiOiIxMSJ9.QcHBiitvAMVnq1ClG8NH_QhxN8tRSMWpcYYHrd2xSE4');

-- --------------------------------------------------------

--
-- Table structure for table `encrypt`
--

CREATE TABLE `encrypt` (
  `id` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `keynumber` text NOT NULL,
  `stat_conn` int(11) NOT NULL,
  `feedback` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `encrypt`
--

INSERT INTO `encrypt` (`id`, `waktu`, `keynumber`, `stat_conn`, `feedback`) VALUES
(1, '2021-04-27 10:23:02', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJpc21haWxtYXJ6dWtpIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjIzOjAyIiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.bn-jw7_1Xt80Vifs6N59hpjxBiiAZz5RMeSF6EGyL1I', 1, ''),
(2, '2021-04-27 10:24:16', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJpc21haWxtYXJ6dWtpYWxpIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjI0OjE2IiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.Vr4KIDb4BQK1opFt24IVp2sNdSchZExaLXMs1eqQHck', 1, ''),
(3, '2021-04-27 10:26:48', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOiJkYWdvIiwid2FrdHUiOiIyMDIxLTA0LTI3IDEwOjI2OjQ4IiwicG0xMCI6IjEyMyIsInBtMjUiOiIxMiIsInNvMiI6IjEwIiwiY28iOiIxNSIsIm8zIjoiMTIiLCJubzIiOiIzMjIxIiwiaGMiOiIxMiIsIndzIjoiMTIiLCJ3ZCI6IjIzMiIsInN0YXRfcG0xMCI6bnVsbCwic3RhdF9wbTI1IjpudWxsLCJzdGF0X3NvMiI6bnVsbCwic3RhdF9jbyI6bnVsbCwic3RhdF9vMyI6bnVsbCwic3RhdF9ubzIiOm51bGwsInN0YXRfaGMiOm51bGwsImh1bWlkaXR5IjoiMSIsInRlbXBlcmF0dXIiOiIxIiwicHJlc3N1cmUiOm51bGwsInNyIjoiMTEiLCJyYWluX2ludGVuc2l0eSI6IjExIn0.LxndgeAKU0f4kL-BuHR3jIcXQNxqYWnzdaojBj6HrxA', 1, 'sukses'),
(4, '2021-04-27 11:03:28', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIwMDAwMDAwMDAwMTYsImlkX3N0YXNpdW4iOm51bGwsIndha3R1IjoiMjAyMS0wNC0yNyAxMTowMzoyOCIsInBtMTAiOiIxMjMiLCJwbTI1IjoiMTIiLCJzbzIiOiIxMCIsImNvIjoiMTUiLCJvMyI6IjEyIiwibm8yIjoiMzIyMSIsImhjIjoiMTIiLCJ3cyI6IjEyIiwid2QiOiIyMzIiLCJzdGF0X3BtMTAiOm51bGwsInN0YXRfcG0yNSI6bnVsbCwic3RhdF9zbzIiOm51bGwsInN0YXRfY28iOm51bGwsInN0YXRfbzMiOm51bGwsInN0YXRfbm8yIjpudWxsLCJzdGF0X2hjIjpudWxsLCJodW1pZGl0eSI6IjEiLCJ0ZW1wZXJhdHVyIjoiMSIsInByZXNzdXJlIjpudWxsLCJzciI6IjExIiwicmFpbl9pbnRlbnNpdHkiOiIxMSJ9.QcHBiitvAMVnq1ClG8NH_QhxN8tRSMWpcYYHrd2xSE4', 1, 'sukses');

-- --------------------------------------------------------

--
-- Table structure for table `secretkey`
--

CREATE TABLE `secretkey` (
  `id` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `secretkey`
--

INSERT INTO `secretkey` (`id`, `waktu`, `key`) VALUES
(1, '2021-04-27 10:26:00', 'ujikonektivitasklhk');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `token`) VALUES
('dani', 'xxxx', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `encrypt`
--
ALTER TABLE `encrypt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `encrypt`
--
ALTER TABLE `encrypt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
